from unittest import TestCase
from mock import patch, MagicMock
from click.testing import CliRunner
from mongo_migrations.commands import mongo_migrations


class InitTests(TestCase):

    def test_commands_registered(self):
        self.commands = mongo_migrations.commands.keys()
        self.assertTrue('status' in self.commands)
        self.assertTrue('create' in self.commands)
        self.assertTrue('up' in self.commands)
        self.assertTrue('down' in self.commands)


class CommandsTests(TestCase):

    def setUp(self):
        self.mock_migrations_patcher = patch("mongo_migrations.commands.Migrations")
        self.addCleanup(self.mock_migrations_patcher.stop)
        self.mock_migrations = self.mock_migrations_patcher.start()
        self.mock_migrations.return_value = MagicMock()

        self.mock_db_patcher = patch("mongo_migrations.commands.imp.load_source")
        self.addCleanup(self.mock_db_patcher.stop)
        self.mock_db = self.mock_db_patcher.start()

        self.commands = mongo_migrations.commands
        self.runner = CliRunner()

    @patch('__builtin__.open')
    def test_init(self, mock_open):
        mock_open.return_value = MagicMock()
        result = self.runner.invoke(self.commands['init'])
        self.assertEqual(0, result.exit_code)

    def test_status(self):
        result = self.runner.invoke(self.commands['status'])
        self.assertEqual(0, result.exit_code)
        self.assertTrue(self.mock_migrations.return_value.show_status.called)

    def test_create(self):
        result = self.runner.invoke(self.commands['create'])
        self.assertEqual(2, result.exit_code)

        result = self.runner.invoke(self.commands['create'],
                                    ['name'])
        self.assertEqual(0, result.exit_code)
        self.assertTrue(self.mock_migrations.return_value.create.called)

    def test_up(self):
        result = self.runner.invoke(self.commands['up'])
        self.assertEqual(0, result.exit_code)
        self.assertTrue(self.mock_migrations.return_value.up.called)

        result = self.runner.invoke(self.commands['up'], ['1'])
        self.assertEqual(0, result.exit_code)
        self.assertTrue(self.mock_migrations.return_value.up.called)

        result = self.runner.invoke(self.commands['up'],
                                    ['1', '--fake'])
        self.assertEqual(0, result.exit_code)
        self.assertTrue(self.mock_migrations.return_value.up.called)

    def test_down(self):
        # result = self.runner.invoke(self.commands['down'])
        # self.assertEqual(2, result.exit_code)

        result = self.runner.invoke(self.commands['down'], ['1'])
        self.assertEqual(0, result.exit_code)
        self.assertTrue(self.mock_migrations.return_value.down.called)
